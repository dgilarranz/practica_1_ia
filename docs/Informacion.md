Copyright 2021 David Gilarranz Ortega
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Colitis Ulcerosa
Enfermedad inflamatoria intestinal crónica que tiene afectación continua de colon y recto. Se extiende en sentido
proximal (desde abajo hacia arriba, comenzando en el recto y avanzando por el colon) y solo afecta a la mucosa.

Puede ser:

- **Colitis Distal**: hasta el ángulo esplénico del colon.
- **Colitis Extensa**: se extiende más allá del ángulo esplénico del colon.

### Estadificación del Paciente en Función de la Gravedad
Para identificar la gravedad de un brote se emplea el índice Truelove-Witts modificado (versión empleada
en ensayos clínicos españoles):

|Parámetros|1 punto|2 puntos|3 puntos|
|:---------|:-----:|:------:|:------:|
|Número de deposiciones al día|<4|4-5|>5|
|Sangre en Heces|no hay/poca cantidad|cantidad moderada|cantidad elevada|
|Temperatura Axilar|<37|37-37,5|>37,5|
|Frecuencia cardíaca (lpm)|<80|80-90|>90|
|Hemoglobina (g/L) Hombres|>14|10-14|<10|
|Hemoglobina (g/L) Mujeres|>12|9-12|<9|
|VSG (mm/h)|<15|15-30|>30|

*lpm*: latidos por minuto, *VSG*: Velocidad de Sedimentación Globular

Se suman los puntos obtenidos en función de los síntomas del paciente, pudiendo distinguir entre los siguientes grados
de afectación:

- **6 puntos**: remisión / enfermedad inactiva.
- **7 - 10 puntos**: brote leve.
- **11 - 14 puntos**: brote moderado.
- **15 - 18 puntos**: brote grave.

### Tratamiento en función de la Actividad
Se aplican las siguientes reglas:

- Si el brote es leve o moderado:
	- Menos de 40kg: 5-ASA 1 o 2 gramos/día.
	- A partir de 40kg: 5-ASA 2 o 3 gramos/día.
	- Vía:
		- **Oral**: Si tiene **colitis extensa**.
		- **Tópica**: Si tiene **colitis distal**.
- Si no se alcanza remisión con 5-ASA:
	- Prednisona oral: 40 a 60 mg/día (en función del peso).
- Si con Prednisona o 5-ASA se alcanza remisión:
	- Si tiene **colitis extensa**: 5-ASA 1.5/2 g/día.
	- Si tiene **colitis distal**: 1 gramo vía tópica (cada 48h)
- Si con Prednisona Oral se genera **corticodependencia**:
	- Azatioprina: 2/3mg por kg de peso del paciente al día
- Si con Azatioprina se alcanza remisión:
	- Se permanece con el tratamiento de azatioprina.
- Si con Azatioprina no se alcanza remisión:
	- INFLIXIMAB, ADALIMUMAB, GOLIMUMAB, VEDOLIZUMAB (fármacos biológicos).
- Si con Fármacos biológicos se alcanza remisión, se conserva el tratamiento sin cambios.
- Si con Fármacos biológicos no se alcanza remisión, se debe realizar cirugía (colectomía).
- Si con Prednisona Oral no se alcanza remisión:
	- Se trata como brote grave.

- Si el brote es grave (o no se alcanza remisión con Prednisona Oral):
	- Prednisona (1mg/kg de peso del paciente vía intravenosa).
	- Valorar la posibilidad de nutrición enteral o parenteral.
- Si con Prednisona Intravenosa se alcanza remisión, mismo tratamiento que al alcanzar remisión con 5-ASA o Prednisona
oral.
- Si con Prednisona Intravenosa no se alcanza remisión (pacientes corticorrefractarios):
	- Descartar complicaciones (sobreinfección por *C difficile* o Citomegalovirus - CMV) con análisis de sangre
	y heces.
- Si se detecta *C difficile*:
	- Tratamiento antibiótico con metronidazol o vancomicina.
- Si se detecta CMV:
	- Tratamiento antiviral con foscarnet u otros.
- Si no se detectan complicaciones:
	- Fármacos Biológicos. Aplicar mismas reglas que antes para estos fármacos.
- Si el brote es grave y el paciente es corticodependiente:
	- Azatioprina (igual que en caso de brote leve/moderado en paciente corticodependiente).

**Nota**: El sistema no considera la posibilidad de que un pacentiente tenga simultáneamente corticodependencia y complicaciones

