Copyright 2021 David Gilarranz Ortega
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

### Hombre con brote leve de colitis distal y 67kg de peso, sin tratamientos previos:

Objeto JSON para representar el paciente:

```json
{
	"sexo": "hombre",
	"peso": "67",
	"tipo_colitis": "distal",
	"sintomas": [ "5", "no", "36.5", "74", "16", "10" ]
}
```
Respuesta: 

```json
{"tratamiento":"5-ASA tópica: 2 o 3 gramos/día"}
```

### Mujer con brote leve de colitis extensa, 38 kg de peso y ningún tratamiento previo

Objeto JSON:

```json
{
	"sexo": "mujer",
	"peso": "38",
	"tipo_colitis": "extensa",
	"sintomas": [ "5", "poca", "35", "79", "13", "14" ]
}
```
Respuesta: 

```json
{"tratamiento":"5-ASA oral: 1 o 2 gramos/día"}
```

### Hombre con brote leve y tratamiento previo de 5-ASA, que no alcanza remisión

Objeto JSON:

```json
{
	"sexo":"hombre",
	"peso":"81",
	"tipo_colitis":"distal",
	"sintomas": ["3", "moderada", "37", "78", "15", "13" ],
	"tratamiento_previo":"pentasa"
}
``` 
Respuesta:

```json
{"tratamiento":"Prednisona oral: 40 a 60 mg/día"}
```

### Mujer con brote moderado de colitis extensa que alcanza remisión con prednisona oral

Objeto JSON:

```json
{
	"sexo":"mujer",
	"peso":"58",
	"tipo_colitis":"extensa",
	"sintomas": ["3", "no", "36", "69", "14", "14"],
	"tratamiento_previo":"prednisona_oral"
}
```
Respuesta:

```json
{"tratamiento":"5-ASA oral: 1.5 o 2 gramos/día"}
```

### Hombre con brote moderado que genera corticodependencia con un tratamiento de prednisona oral

Objeto JSON:

```json
{
	"sexo":"hombre",
	"peso":"55",
	"tipo_colitis":"distal",
	"sintomas": ["5", "poca", "38", "85", "12", "23"],
	"tratamiento_previo":"prednisona_oral",
	"corticodependencia":"corticodependencia"
}
```
Respuesta:

```json
{"tratamiento":"Azatioprina: 110 a 165 mg/día"}
```

### Mujer con brote moderado que alcanza remisión con tratamiento de Azatioprina

Objeto JSON:

```json
{
	"sexo":"mujer",
	"peso":"57",
	"tipo_colitis":"extensa",
	"sintomas": ["2", "no", "36", "75", "14", "13"],
	"tratamiento_previo":"azatioprina"
}
```
Respuesta:

```json
{"tratamiento":"Azatioprina: 114 a 171 mg/día"}
```

### Hombre con brote leve que no logra alcanzar remisión con Azatiopriona

Objeto JSON:

```json
{
	"sexo":"hombre",
	"peso":"66",
	"tipo_colitis":"distal",
	"sintomas": ["5", "poca", "35", "81", "17", "14"], 
	"tratamiento_previo":"azatioprina"
}
```
Respuesta:

```json
{"tratamiento":"Fármacos Biológicos: INFLIXIMAB, ADALIMUMAB, GOLIMUMAB, VEDOLIZUMAB"}
```

### Mujer con brote moderado que no alcanza remisión con tratamientos biológicos

Objeto JSON:

```json
{
	"sexo":"mujer",
	"peso":"51",
	"tipo_colitis":"extensa",
	"sintomas": ["5", "moderada", "37.6", "89", "10", "16"],
	"tratamiento_previo":"biologicos"
}
```
Respuesta:

```json
{"tratamiento":"Realizar cirujía: colectomía"}
```

### Hombre con colitis extensa que alcanza remisión con tratamientos biológicos

Objeto JSON:

```json
{
	"sexo":"hombre",
	"peso":"60",
	"tipo_colitis":"extensa",
	"sintomas": ["1", "no", "35.7", "70", "20", "9"],
	"tratamiento_previo":"biologicos"
}
```
Respuesta:

```json
{"tratamiento":"Fármacos Biológicos: INFLIXIMAB, ADALIMUMAB, GOLIMUMAB, VEDOLIZUMAB"}
```

### Mujer con brote leve que no alcanza remisión con prednisona oral

Objeto JSON:

```json
{
	"sexo":"mujer",
	"peso":"60",
	"tipo_colitis":"distal",
	"sintomas": ["5", "poca", "36", "75", "13", "14"],
	"tratamiento_previo":"prednisona_oral"
}
```
Respuesta:

```json
{"tratamiento":"Prednisona intravenosa: 60 mg/día"}
```

### Hombre que alcanza remisión con prednisona intravenosa

Objeto JSON:

```json
{
	"sexo":"hombre",
	"peso":"83",
	"tipo_colitis":"extensa",
	"sintomas": ["7", "elevada", "38.6", "94", "9", "37"],
	"tratamiento_previo":"prednisona_intravenosa"
}
```
Respuesta:

```json
{ "tratamiento":"Fármacos Biológicos: INFLIXIMAB, ADALIMUMAB, GOLIMUMAB, VEDOLIZUMAB"}
```

### Mujer con brote grave que no alcanza remisión por C Difficile

Objeto JSON:

```json
{
	"sexo":"mujer",
	"peso":"74",
	"tipo_colitis":"distal",
	"sintomas": ["6", "elevada", "39", "97", "5", "34"],
	"tratamiento_previo":"prednisona_intravenosa",
	"complicaciones":"c_difficile"
}
```
Respuesta:

```json
{"tratamiento":"Antibiótico: Metronizador o Vancomicina"}
```

### Hombre con brote grave que no alcanza remisión por CMV

Objeto JSON:

```json
{
	"sexo":"hombre",
	"peso":"59",
	"tipo_colitis":"distal",
	"sintomas": ["8", "elevada", "37.9", "102", "7", "41"],
	"tratamiento_previo":"prednisona_intravenosa",
	"complicaciones":"cmv"
}
```
Respuesta:

```json
{"tratamiento":"Antiviral: Foscarnet / Otros"}
```

### Mujer con brote grave y tratamiento previo de prednisona intravenosa corticodependiente

```json
{
	"sexo":"mujer",
	"peso":"39",
	"tipo_colitis":"extensa",
	"sintomas": ["9", "elevada", "38.5", "99", "8", "34"],
	"tratamiento_previo":"prednisona_intravenosa",
	"corticodependencia":"corticodependiente"
}
```
Respuesta:

```json
{"tratamiento":"Azatioprina: 78 a 117 mg/día"}
```
