% Copyright 2021 David Gilarranz Ortega
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
% http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% Incluimos los contenidos del fichero diagnóstico.pl
:- ["diagnostico.pl"].

% PACIENTE EN REMISIÓN Y SIN TRATAMIENTOS PREVIOS
%
% Se trata de un caso excepcional puesto que este paciente no tiene colitis ulcerosa. Este caso existe para evitar 
% errores del servidor ante introducción de datos de pacientes sanos
tratamiento([_Sexo, _Peso, _Tipo, _Sintomas | [] ], remision, "No hay tratamiento aplicable").

% PACIENTE CON BROTE LEVE O MODERADO SIN TRATAMIENTOS PREVIOS
%
% Si el paciente tiene colitis extensa y pesa menos de 40kg --> 5-ASA 1 o 2 gramos al día, vía oral
tratamiento([_Sexo, Peso, extensa, _Sintomas | []], broteLeve, "5-ASA oral: 1 o 2 gramos/día"):- Peso < 40. 
tratamiento([_Sexo, Peso, extensa, _Sintomas | []], broteModerado, "5-ASA oral: 1 o 2 gramos/día"):- Peso < 40. 

% Si el paciente tiene colitis extensa y pesa 40kg o más --> 5-ASA 2 o 3 gramos al día, vía oral
tratamiento([_Sexo, Peso, extensa, _Sintomas | []], broteLeve, "5-ASA oral: 2 o 3 gramos/día"):- Peso >= 40. 
tratamiento([_Sexo, Peso, extensa, _Sintomas | []], broteModerado, "5-ASA oral: 2 o 3 gramos/día"):- Peso >= 40. 

% Si el paciente tiene colitis distal y pesa menos de 40kg --> 5-ASA 1 o 2 gramos al día, vía tópica 
tratamiento([_Sexo, Peso, distal, _Sintomas | []], broteLeve, "5-ASA tópica: 1 o 2 gramos/día"):- Peso < 40. 
tratamiento([_Sexo, Peso, distal, _Sintomas | []], broteModerado, "5-ASA tópica: 1 o 2 gramos/día"):- Peso < 40. 

% Si el paciente tiene colitis distal y pesa 40kg o más --> 5-ASA 2 o 3 gramos al día, vía tópica 
tratamiento([_Sexo, Peso, distal, _Sintomas | []], broteLeve, "5-ASA tópica: 2 o 3 gramos/día"):- Peso >= 40. 
tratamiento([_Sexo, Peso, distal, _Sintomas | []], broteModerado, "5-ASA tópica: 2 o 3 gramos/día"):- Peso >= 40. 

% PACIENTE CON BROTE LEVE O MODERADO Y TRATAMIENTO PREVIO DE 5-ASA
%
% Si el paciente tiene un brote leve y ha sido tratado previamente con pentasa, significa que no se
% ha logrado entrar en remisión. En este caso, se administra Prednisona oral (entre 40 y 60 mg)
tratamiento([_Sexo, _Peso, _Tipo, _Sintomas, pentasa | _Resto], broteLeve, "Prednisona oral: 40 a 60 mg/día").
tratamiento([_Sexo, _Peso, _Tipo, _Sintomas, pentasa | _Resto], broteModerado, "Prednisona oral: 40 a 60 mg/día").


% PACIENTE CON BROTE LEVE O MODERADO, TRATAMIENTO PREVIO DE PREDNISONA ORAL Y CORTICODEPENDENCIA
%
% Si el paciente genera coticodependencia con el tratamiento de prednisona oral, se cambia el tratamiento a 
% azatioprina, independientemente del diagnóstico del paciente.
tratamiento([_Sexo, Peso, _Tipo, _Sintomas, prednisona_oral, corticodependencia], _Diagnostico, Tratamiento):-
	Dosis_Minima is Peso * 2, % Calculamos la dosis mínima de Azatioprina en función del peso del paciente
	Dosis_Maxima is Peso * 3, % Calculamos la dosis máxima
	atomic_list_concat(["Azatioprina: ", Dosis_Minima, " a ", Dosis_Maxima, " mg/día"], Tratamiento). 

% PACIENTE QUE LOGRA ALCANZAR REMISIÓN CON TRATAMIENTO DE 5-ASA o PREDNISONA ORAL
%
% Si el paciente logra alcanzar remisión con un tratamiento de 5-ASA o Prednisona oral, se pone un tratamiento
% de estabilización en función del tipo de colitis que tenga
tratamiento([_Sexo, _Peso, extensa, _Sintomas, pentasa | _Resto], remision, "5-ASA oral: 1.5 o 2 gramos/día").
tratamiento([_Sexo, _Peso, extensa, _Sintomas, prednisona_oral | _Resto], remision, "5-ASA oral: 1.5 o 2 gramos/día").

tratamiento([_Sexo, _Peso, distal, _Sintomas, pentasa | _Resto], remision, "5-ASA tópica: 1g/48h").
tratamiento([_Sexo, _Peso, distal, _Sintomas, prednisona_oral | _Resto], remision, "5-ASA tópica: 1g/48h").

% PACIENTE CON TRATAMIENTO DE AZATIOPRINA QUE LOGRA ALCANZAR REMISIÓN
%
% Si un paciente tiene un tratamieneto de Azatioprina y logra entrar en remisión, permanece con ese tratamiento
tratamiento([Sexo, Peso, Tipo, Sintomas, azatioprina | _Resto], remision, Tratamiento):-
	% Mismo tratamiento que si se hubiera generado corticodependencia con prednisona oral
	tratamiento([Sexo, Peso, Tipo, Sintomas, prednisona_oral, corticodependencia], remision, Tratamiento),
	!.	% Corte para garantizar que si el paciente está en remisión, no se continúe por el caso inferior

% PACIENTE CON TRATAMIENTO DE AZATIOPRINA QUE NO LOGRA ALCANZAR REMISIÓN
%
% No hace falta comprobar si el paciente está en remisión o no puesto que el corte ya garantiza la exclusión mutua 
tratamiento([_Sexo, _Peso, _Tipo, _Sintomas, azatioprina | _Resto], _Brote,
	"Fármacos Biológicos: INFLIXIMAB, ADALIMUMAB, GOLIMUMAB, VEDOLIZUMAB").

% PACIENTE CON TRATAMIENTO DE FÁRMACOS BIOLÓGICOS QUE LOGRA ALCANZAR REMISIÓN
% 
% Si un paciente tratado con fármacos biológicos logra alcanzar remisión, se conserva dicho tratamiento
tratamiento([_Sexo, _Peso, _Tipo, _Sintomas, biologicos | _Resto], remision, 
	"Fármacos Biológicos: INFLIXIMAB, ADALIMUMAB, GOLIMUMAB, VEDOLIZUMAB"):-
	!.	% Corte para garantizar exlusión con el caso de que no se alcance remisión

% PACIENTE CON TRATAMIENTO DE FÁRMACOS BIOLÓGICOS QUE NO LOGRA ALCANZAR REMISIÓN
%
% Si el paciente lo logra alcanzar remisión, se deberá realizar una colectomía. No comprobamos si se alcanza o no remisión
% porque el corte del predicado anterior garantiza casos disjuntos (al estar antes en el documento, si se cumple dicho
% caso el corte garantizará que no se cumpla este).
tratamiento([_Sexo, _Peso, _Tipo, _Sintomas, biologicos | _Resto], _Brote, "Realizar cirujía: colectomía"). 

% PACIENTE QUE NO LOGRA ALCANZAR REMISIÓN CON PREDNISONA ORAL
%
% Si el paciente no logra alcanzar remisión con un tratamiento de prednisona oral, se considerá que tiene un brote grave,
% independientemente de si su brote es leve o moderado.
tratamiento([Sexo, Peso, Tipo, Sintomas, prednisona_oral | Resto], broteLeve, Tratamiento):-
	% Llamamos recursivamente al predicado, modificando el tipo de brote del paciente para aplicar las reglas necesarias
	tratamiento([Sexo, Peso, Tipo, Sintomas, prednisona_oral | Resto], broteGrave, Tratamiento).

tratamiento([Sexo, Peso, Tipo, Sintomas, prednisona_oral | Resto], broteModerado, Tratamiento):-
	% Llamamos recursivamente al predicado, modificando el tipo de brote del paciente para aplicar las reglas necesarias
	tratamiento([Sexo, Peso, Tipo, Sintomas, prednisona_oral | Resto], broteGrave, Tratamiento).

% PACIENTE CON BROTE GRAVE CORTICODEPENDIENTE
%
% Si el paciente tiene un brote grave y es corticodependiente, se aplica el mismo tratamiento que si es corticodependiente
% tras recibir prednisona oral
tratamiento([Sexo, Peso, Tipo, Sintomas, prednisona_intravenosa, corticodependiente], broteGrave, Tratamiento):-
	% Llamamos recursivamente al predicado correspondiente al tratamiento de los pacientes corticodependientes
	tratamiento([Sexo, Peso, Tipo, Sintomas, prednisona_oral, corticodependencia], broteGrave, Tratamiento),
	!.	% Corte para garantizar casos disjuntos

% PACIENTE QUE NO ALCANZA REMISIÓN CON PREDNISONA INTRAVENOSA Y NO TIENE COMPLICACIONES
%
% Si el paciente no alcanza remisión con prednisona intravenosa y no tiene complicaciones, recibirá fármacos biológicos
tratamiento([_Sexo, _Peso, _Tipo, _Sintomas, prednisona_intravenosa | []], Brote, 
	"Fármacos Biológicos: INFLIXIMAB, ADALIMUMAB, GOLIMUMAB, VEDOLIZUMAB"):-
		Brote \= remision,
		!.

% PACIENTE QUE NO ALCANZA REMISIÓN CON PREDNISONA INTRAVENOSA Y TIENE COMPLICACIONES POR C DIFFICILE
%
% Si el paciente no alcanza remisión con prednisona intravenosa y tiene complicaciones por C Difficile, se deberá aplicar un
% tratamiento antibiótico con metronizadol o vancomicina
tratamiento([_Sexo, _Peso, _Tipo, _Sintomas, prednisona_intravenosa, c_difficile], _Brote, 
	"Antibiótico: Metronizador o Vancomicina"):- !.

% PACIENTE QUE NO ALCANZA REMISIÓN CON PREDNISONA INTRAVENOSA Y TIENE COMPLICACIONES POR CMV
%
% Si el paciente no alcanza remisión con prednisona intravenosa y tiene complicaciones por CMV, se deberá aplicar un
% tratamiento antiviral con Foscarnet u otros
tratamiento([_Sexo, _Peso, _Tipo, _Sintomas, prednisona_intravenosa, cmv], _Brote, "Antiviral: Foscarnet / Otros"):- !.

% PACIENTE CON BROTE GRAVE -> CASO GENERAL: NO HA SIDO TRATATADO CON PREDNISONA INTRAVENOSA o NO ES CORTICODEPENDIENTE
%
% Situamos este caso en el final del fichero para dar prioridad a todos los anteriores. Los cortes aseguran que los casos
% son disjuntos
%
% Si el paciente tiene un brote grave y no aplica ninguna de las casuísticas más específicas, se adopta un tratamiento de
% prednisona intravenosa en función del peso del paciente
tratamiento([_Sexo, Peso | _Resto], broteGrave, Tratamiento):-
       atomic_list_concat(["Prednisona intravenosa: ", Peso, " mg/día"], Tratamiento).	
