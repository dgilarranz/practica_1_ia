% Copyright 2021 David Gilarranz Ortega
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
% http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% Información sobre el paciente:
% El paciente se tratará como una lista con la siguiente estructura:
% 	Paciente ::= [ Sexo, Peso, Tipo_Colitis, Sintomas, Tratamiento_Previo, Corticodependencia, Complicaciones ]
% Donde:
% 	Sexo ::= hombre | mujer
% 	Peso ::= Número entero positivo
% 	Tipo_Colitis ::= extensa | distal
% 	Sintomas ::= [ 
% 			Número de Deposiciones al Día,
% 		     	Sangre en Heces,
% 		     	Temperatura Auxiliar,
% 		     	Frecuencia Cardíaca (bpm),
% 		     	Hemoglobina (g/L),
% 		     	VSG (mm/h)
% 		     ]
%		Con Sangre_en_Heces ::= no | poca | moderada | elevada
% 	Tratamiento_Previo ::= pentasa | prednisona_oral | prednisona_intravenosa 
% 				| azatioprina | biologicos | λ
% 	Corticodependencia ::= corticodependencia | λ
% 	Complicaciones ::= c_difficile | cmv | λ

% REGLAS de DIAGNÓSTICO

% Reglas para determinar el estado del paciente
diagnostico(Paciente, remision):- remision(Paciente).
diagnostico(Paciente, broteLeve):- broteLeve(Paciente).
diagnostico(Paciente, broteModerado):- broteModerado(Paciente).
diagnostico(Paciente, broteGrave):- broteGrave(Paciente).

% Un paciente está en remisión si el valor del indice Truelove-Witts es 6
%
% 	Para enviar sólo los datos relevantes al índice Truelove-Witts, se extraen de la lista los elementos
% 	correspondientes y se utilizan sólo los necesarios, teniendo en cuenta la estructura del paciente
% 	definida.
remision([Sexo, _Peso, _Tipo_Colitis, Sintomas | _Resto]):- 
	truelove_witts(Sexo, Sintomas, 6).

% Un paciente tiene un brote leve si el valor índice Truelove-Witts está entre 7 y 10
broteLeve([Sexo, _Peso, _Tipo_Colitis, Sintomas | _Resto]):- 
	truelove_witts(Sexo, Sintomas, TW),
	TW >= 7, TW =< 10.

% Un paciente tiene un brote moderado si el valor del índice TW está entre 11 y 14
broteModerado([Sexo, _Peso, _Tipo_Colitis, Sintomas | _Resto]):-
	truelove_witts(Sexo, Sintomas, TW),
	TW >= 11, TW =< 14.

% Un paciente tiene un brote grave si el valor del índice TW está entre 15 y 18
broteGrave([Sexo, _Peso, _Tipo_Colitis, Sintomas | _Resto]):-
	truelove_witts(Sexo, Sintomas, TW),
	TW >= 15, TW =< 18.

% TW es el valor del índice Truelove-Witts modificado para unos parámetros determinados si es el resultado
% de sumar los puntos obtenidos al evaluar cada parámetro
truelove_witts(Sexo, [Deposiciones, Sangre, Temperatura, BPM, Hemoglobina, VSG | []], TW):-
	evaluarDeposiciones(Deposiciones, D),
	evaluarSangre(Sangre, S),
	evaluarTemperatura(Temperatura, T),
	evaluarBPM(BPM, B),
	evaluarHemoglobina(Hemoglobina, Sexo, H),
	evaluarVSG(VSG, V),
	TW is D + S + T + B + H + V.

% En función del número de deposiciones al día del paciente, se recibe 1, 2 o 3 puntos
evaluarDeposiciones(Dep, 1):- Dep < 4, !.	% Menos de 4 deposiciones -> 1 punto
evaluarDeposiciones(Dep, 3):- Dep > 5, !. 	% Más de 5 deposiciones -> 3 puntos
evaluarDeposiciones(_Dep, 2).			% En cualquier otro caso (4 o 5) -> 2 puntos 

% En función de la cantidad de sangre en heces, se recibe 1, 2 o 3 puntos
evaluarSangre(no, 1).
evaluarSangre(poca, 1).
evaluarSangre(moderada, 2).
evaluarSangre(elevada, 3).

% En función de la temperatura axilar, se reciben 1, 2 o 3 puntos
evaluarTemperatura(T, 1):- T < 37, !.		% Si T < 37º -> 1 punto
evaluarTemperatura(T, 3):- T > 37.5, !.		% Si T > 37.5ª -> 3 puntos
evaluarTemperatura(_T, 2).			% En cualquier otro caso (37 <= T <= 37.5) -> 2 puntos

% En función de la frecuencia cardíaca del paciente, se reciben 1, 2 o 3 puntos
evaluarBPM(BPM, 1):- BPM < 80, !.		% Si BPM < 80 -> 1 punto
evaluarBPM(BPM, 3):- BPM > 90, !.		% Si BPM > 90 -> 3 puntos
evaluarBPM(_BPM, 2).				% En cualquier otro caso (80 <= BPM <= 90) -> 2 puntos

% En función del sexo y la hemoglobina del paciente, se reciben 1, 2 o 3 puntos
evaluarHemoglobina(H, hombre, 1):- H > 14, !.	% Si el paciente es un hombre y H > 14 -> 1 punto
evaluarHemoglobina(H, hombre, 3):- H < 10, !.	% Si el paciente es un hombre y H < 14 -> 3 punto
evaluarHemoglobina(_H, hombre, 2).		% Si el paciente es hombre y no se cumplen los casos anteriores -> 2 puntos


evaluarHemoglobina(H, mujer, 1):- H > 12, !.	% Si el paciente es una mujer y H > 12 -> 1 punto
evaluarHemoglobina(H, mujer, 3):- H < 9, !.	% Si el paciente es una mujer y H < 9 -> 3 punto
evaluarHemoglobina(_H, mujer, 2).		% Si el paciente es una mujer y no se cumplen los casos anteriores -> 2 puntos

% En función de la Velocidad de Sedimentación Globular del paciente, se recibe 1, 2 o 3 puntos
evaluarVSG(VSG, 1):- VSG < 15, !.		% Si VSG < 15 -> 1 punto
evaluarVSG(VSG, 3):- VSG > 30, !.		% Si VSG > 20 -> 3 puntos
evaluarVSG(_VSG, 2).				% Si no se cumplen los casos anteriores -> 2 puntos		
