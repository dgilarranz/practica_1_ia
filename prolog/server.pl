% Copyright 2021 David Gilarranz Ortega
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
% http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% Incluimos los ficheros .pl necesarios
:- ["diagnostico.pl"].
:- ["tratamiento.pl"].

% Fichero que contiene el código necesario para el establecimiento del servidor y la gestión de peticiones

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_json)).

:- initialization(
	http_server(http_dispatch, [port(5000)])	% Al cargarse este fichero, se iniciará un servidor en el puerto 5000
).			

:- http_handler(root(tratamiento), manejar_peticion, []).	% Establecemos un manejador para peticiones de tratamientos


% Predicado que maneja la petición:
% 	- Crea la lista correspondiente al paciente en función de los datos recibidos
% 	- Determina el diagnóstico del paciente
% 	- Selecciona el tratamineto
% 	- Responde con el tratamiento
manejar_peticion(Request):-
	http_read_json_dict(Request, PacienteJSON),		% Obtenemos una representación en JSON del paciente
	crear_paciente(PacienteJSON, Paciente),			% Convertimos el paciente de JSON a la lista empleada por el sistema
	diagnostico(Paciente, Diagnostico),			% Diagnosticamos al paciente
	tratamiento(Paciente, Diagnostico, Tratamiento),	% Obtenemos el tratamiento adecuado para el paciente
	reply_json_dict(_{tratamiento:Tratamiento}).		% Respondemos con un diccionario compuesto por el tratamiento	

% PREDICADOS QUE GENERAN LA LISTA DEL PACIENTE
%
% En el caso de que se reciba un paciente sin tratamientos previos, corticodependencia ni complicaciones
crear_paciente(_{sexo:Sexo_json, peso:Peso_json, tipo_colitis:Tipo_json, sintomas:Sintomas_json}, [Sexo, Peso, Tipo, Sintomas]):-
	atom_string(Sexo, Sexo_json),
	atom_number(Peso_json, Peso),
	atom_string(Tipo, Tipo_json),
	convertir_sintomas(Sintomas_json, Sintomas).

% En el caso de que se reciba un paciente con tratamiento previo, sin corticodependencia ni complicaciones
crear_paciente(
	_{sexo:Sexo_json, peso:Peso_json, tipo_colitis:Tipo_json, sintomas:Sintomas_json, tratamiento_previo:TP_json}, 
	[Sexo, Peso, Tipo, Sintomas, TratamientoPrevio]
):-
	atom_string(Sexo, Sexo_json),
	atom_number(Peso_json, Peso),
	atom_string(Tipo, Tipo_json),
	atom_string(TratamientoPrevio, TP_json),
	convertir_sintomas(Sintomas_json, Sintomas).

% En el caso de que se reciba un paciente con tratamiento previo y corticodependencia
crear_paciente(
	_{sexo:Sexo_json, peso:Peso_json, tipo_colitis:Tipo_json, sintomas:Sintomas_json, tratamiento_previo:TP_json, corticodependencia:Cortico_json}, 
	[Sexo, Peso, Tipo, Sintomas, TratamientoPrevio, Corticodependencia]
):-
	atom_string(Sexo, Sexo_json),
	atom_number(Peso_json, Peso),
	atom_string(Tipo, Tipo_json),
	atom_string(TratamientoPrevio, TP_json),
	atom_string(Corticodependencia, Cortico_json),
	convertir_sintomas(Sintomas_json, Sintomas).

% En el caso de que se reciba un paciente con tratamiento previo y complicaciones
crear_paciente(
	_{sexo:Sexo_json, peso:Peso_json, tipo_colitis:Tipo_json, sintomas:Sintomas_json, tratamiento_previo:TP_json, complicaciones:Complicaciones_json},
	[Sexo, Peso, Tipo, Sintomas, TratamientoPrevio, Complicaciones]
):-
	atom_string(Sexo, Sexo_json),
	atom_number(Peso_json, Peso),
	atom_string(Tipo, Tipo_json),
	atom_string(TratamientoPrevio, TP_json),
	atom_string(Complicaciones, Complicaciones_json),
	convertir_sintomas(Sintomas_json, Sintomas).

% Predicado que toma la lista de síntomas en formato json y la convierte a los átomos o números correspondientes
convertir_sintomas(
	[Deposiciones_json, Sangre_json, Temp_json, Pulso_json, Hemoglobina_json, VSG_json],
	[Deposiciones, Sangre, Temp, Pulso, Hemoglobina, VSG]
):-
	atom_number(Deposiciones_json, Deposiciones),
	atom_string(Sangre, Sangre_json),
	atom_number(Temp_json, Temp),
	atom_number(Pulso_json, Pulso),
	atom_number(Hemoglobina_json, Hemoglobina),
	atom_number(VSG_json, VSG).

