Copyright 2021 David Gilarranz Ortega
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

# SISTEMA EXPERTO COLITIS ULCEROSA #

### Problema a Resolver y Descripción de las Reglas en Lenguaje Natural

El propósito del sistema experto es la determinación del tratamiento óptimo para un paciente enfermo de colitis
ulcerosa en función de los síntomas que presente, sus características personales (sexo, peso, etc.) y tratamientos
previos que haya podido tener. Se desarrollará un sistema experto encapsulado en una API REST, que permita a los
usuarios realizar consultas HTTP sobre el tratamiento óptimo para un paciente determinado, codificado en JSON.

La colitis ulcerosa es una enfermedad inflamatoria intestinal crónica que tiene afectación continua de colon y recto. 
Se extiende en sentido proximal (desde abajo hacia arriba, comenzando en el recto y avanzando por el colon) y solo afecta 
a la mucosa. Se distinguen los siguientes tipos:

- **Colitis Distal**: afectación hasta el ángulo esplénico del colon.
- **Colitis Extensa**: afectación más allá del ángulo esplénico del colon.

Para más información sobre la colitis ulcerosa, al igual que para ver las reglas de tratamiento, ver el documento 
[Información.md](docs/Informacion.md)
presente en el repositorio.

### Representación de una situación concreta

Como se ha indicado anteriormente, este sistema experto está orientado a prestar un servicio mediante una API REST y, en
consecuencia, no almacena información localmente, sino que recibe como parámetros de la petición los datos necesarios sobre
el paciente. Dichos datos se representan como un objeto JSON con la siguiente estructura:

```json
{
	"sexo":Sexo,
	"peso":Peso,
	"tipo_colitis":Tipo_Colitis,
	"sintomas":Sintomas,
	"tratamiento_previo":Tratamiento_Previo,
	"corticodependencia":Corticodependencia,
	"complicaciones":Complicaciones
}
```
Donde:
```prolog
 	Sexo ::= hombre | mujer
 	Peso ::= Número entero positivo
 	Tipo_Colitis ::= extensa | distal
 	Sintomas ::= [
			Número de Deposiciones al Día,
 		     	Sangre en Heces,
 		     	Temperatura Auxiliar,
 		     	Frecuencia Cardíaca (bpm),
 		     	Hemoglobina (g/L),
 		     	VSG (mm/h)
 		     ]
		Con Sangre_en_Heces ::= no | poca | moderada | elevada
 	Tratamiento_Previo ::= pentasa | prednisona_oral | prednisona_intravenosa 
 				| azatioprina | biologicos | λ
 	Corticodependencia ::= corticodependencia | λ
 	Complicaciones ::= c_difficile | cmv | λ
```
El sistema responderá con un objeto JSON sencillo indicando el tratamiento necesario:
```json
{
	"tratamiento":Tratamiento determinado por el sistema experto
}
```

Por ejemplo, si se desea hacer una consulta sobre un hombre de 67kg, con colitis extensa y con un conjunto de síntomas determinado,
se representará de la siguiente manera:

```json
{
        "sexo": "hombre",
        "peso": "67",
        "tipo_colitis": "distal",
        "sintomas": [ "5", "no", "36.5", "74", "16", "10" ]
}
```
Recibiéndose la siguiente respuesta:
```json
{"tratamiento":"5-ASA tópica: 2 o 3 gramos/día"}
```
Para ver más ejemplos se puede consultar el fichero [ejemplos.md](docs/ejemplos.md).

### Ejecución del sistema

Para lanzar el servidor, basta con cargar el fichero `server.pl` introduciendo el comando:

```console
$ swipl server.pl
```
Este fichero por defecto creará el servidor para gestionar las peticiones, escuchando en el puerto 5000. Para lanzar peticiones
al sistema, se deberá enviar los mensajes a la dirección `localhost:5000/tratamiento`. Ejemplo empleando el comando curl:

```console
$ curl --request POST http://localhost:5000/tratamiento --header "Content-Type: application/json" --data '{
        "sexo": "hombre",
        "peso": "67",
        "tipo_colitis": "distal",
        "sintomas": [ "5", "no", "36.5", "74", "16", "10" ]
}'
```
